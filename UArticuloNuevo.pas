unit UArticuloNuevo;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.FMXUI.Wait,
  Data.DB, FireDAC.Comp.Client, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, FMX.ListBox, FMX.Edit,
  FMX.StdCtrls, FMX.Controls.Presentation, System.Rtti, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.EngExt, Fmx.Bind.DBEngExt, Data.Bind.Components,
  Data.Bind.DBScope;

type
  TArticuloNuevo = class(TForm)
    Conexion: TFDConnection;
    btnGuardar: TButton;
    btnCancelar: TButton;
    Label1: TLabel;
    Label2: TLabel;
    txtNombre: TEdit;
    Label3: TLabel;
    cbLineaArt: TComboBox;
    qryLineasArt: TFDQuery;
    qryLineasArtLINEA_ARTICULO_ID: TIntegerField;
    qryLineasArtNOMBRE: TStringField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkListControlToField1: TLinkListControlToField;
    Label4: TLabel;
    cbUnidades: TComboBox;
    qryUnidades: TFDQuery;
    qryUnidadesUNIDAD_VENTA_ID: TIntegerField;
    qryUnidadesUNIDAD_VENTA: TStringField;
    qryUnidadesCLAVE_SAT: TStringField;
    qryUnidadesSIMBOLO_SAT: TStringField;
    BindSourceDB2: TBindSourceDB;
    LinkListControlToField2: TLinkListControlToField;
    Label5: TLabel;
    qryListaPrecios: TFDQuery;
    cbListaPrecios: TComboBox;
    qryListaPreciosPRECIO_EMPRESA_ID: TIntegerField;
    qryListaPreciosNOMBRE: TStringField;
    BindSourceDB3: TBindSourceDB;
    LinkListControlToField3: TLinkListControlToField;
    txtPrecio: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    txtClave: TEdit;
    Label8: TLabel;
    txtClaveSAT: TEdit;
    txtContenido: TEdit;
    Label9: TLabel;
    lblContenido: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure txtContenidoExit(Sender: TObject);
    procedure cbUnidadesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ArticuloNuevo: TArticuloNuevo;

implementation

{$R *.fmx}

procedure TArticuloNuevo.btnCancelarClick(Sender: TObject);
begin
self.Close;
end;

procedure TArticuloNuevo.btnGuardarClick(Sender: TObject);
var art_id,rol_clave_id:integer;
begin
 try
 art_id:=Conexion.ExecSQLScalar('insert into articulos(ARTICULO_ID,NOMBRE,ESTATUS,LINEA_ARTICULO_ID,UNIDAD_VENTA,UNIDAD_COMPRA,CONTENIDO_UNIDAD_COMPRA,PESO_UNITARIO,PCTJE_ARANCEL,APLICAR_FACTOR_VENTA,FACTOR_VENTA,RED_PRECIO_CON_IMPTO,FACTOR_RED_PRECIO_CON_IMPTO)'+
 ' values (-1,:p1,''A'',:p2,:p3,:p4,:p5,0,0,''N'',0,''N'',0) returning articulo_id',[txtNombre.Text,qryLineasArt.FieldByName('linea_articulo_id').Value,qryUnidades.FieldByName('unidad_venta').Value,qryUnidades.FieldByName('unidad_venta').Value,txtContenido.Text]);
 Conexion.ExecSQL('insert into precios_articulos(PRECIO_ARTICULO_ID,ARTICULO_ID,PRECIO_EMPRESA_ID,PRECIO,MONEDA_ID,MARGEN)'+
 ' values(-1,:p1,:p2,:p3,1,0)',[inttostr(art_id),qryListaPrecios.FieldByName('precio_empresa_id').Value,txtPrecio.Text]);
 rol_clave_id:=Conexion.ExecSQLScalar('select rol_clave_art_id from roles_claves_articulos where nombre=''Clave principal''');
 Conexion.ExecSQL('insert into claves_articulos(CLAVE_ARTICULO_ID,CLAVE_ARTICULO,ARTICULO_ID,ROL_CLAVE_ART_ID)'+
 ' values(-1,:p1,:p2,:p3)',[txtClave.Text,inttostr(art_id),rol_clave_id]);
 if txtClaveSAT.Text<>'' then Conexion.ExecSQL('insert into datos_adicionales(DATOS_ADICIONALES_ID,NOM_TABLA,ELEM_ID,TIPO_REG,CLAVE,DATOS) values(-1,''ARTICULOS'',:p1,2,:P2,NULL)',[inttostr(art_id),txtClaveSAT.Text]);
 Conexion.Commit;
 ShowMessage('El articulo se agrego correctamente.');
 self.close;
 except
 ShowMessage('ERROR: el articulo con ese nombre ya existe o la clave del articulo esta en uso.');
 end;
end;

procedure TArticuloNuevo.cbUnidadesChange(Sender: TObject);
begin
lblContenido.Text:=cbUnidades.Items[cbUnidades.ItemIndex];
end;

procedure TArticuloNuevo.FormShow(Sender: TObject);
begin
qryLineasArt.Open();
qryUnidades.Open();
qryListaPrecios.Open();
txtContenido.text:='1';
lblContenido.Text:=cbUnidades.Items[cbUnidades.ItemIndex];
end;

procedure TArticuloNuevo.txtContenidoExit(Sender: TObject);
var
entero:integer;
begin
 if TryStrToInt(txtContenido.Text,entero)=false then
 begin
  ShowMessage('El contenido debe de ser un numero entero.');
  txtContenido.Text:='1';
 end;
end;

end.
