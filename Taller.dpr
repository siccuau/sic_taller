program Taller;

uses
  System.StartUpCopy,
  FMX.Forms,
  UPrincipal in 'UPrincipal.pas' {FormPrincipal},
  UConexiones in 'UConexiones.pas' {Conexiones},
  UEnvVars in 'UEnvVars.pas',
  ULicencia in 'ULicencia.pas',
  ULogin in 'ULogin.pas' {Login},
  USeleccionaEmpresa in 'USeleccionaEmpresa.pas' {SeleccionaEmpresa},
  WbemScripting_TLB in 'WbemScripting_TLB.pas',
  UArticuloNuevo in 'UArticuloNuevo.pas' {ArticuloNuevo},
  UKitNuevo in 'UKitNuevo.pas' {KitNuevo},
  UKitArticulos in 'UKitArticulos.pas' {KitArticulos},
  UNuevoLMM in 'UNuevoLMM.pas' {NuevoLMM};

{$R *.res}

begin
  Application.Initialize;
    Application.CreateForm(TLogin, Login);
  Application.CreateForm(TFormPrincipal, FormPrincipal);
  Application.CreateForm(TConexiones, Conexiones);
  Application.CreateForm(TSeleccionaEmpresa, SeleccionaEmpresa);
  Application.CreateForm(TArticuloNuevo, ArticuloNuevo);
  Application.CreateForm(TKitNuevo, KitNuevo);
  Application.CreateForm(TKitArticulos, KitArticulos);
  Application.CreateForm(TNuevoLMM, NuevoLMM);
  Application.Run;
end.
