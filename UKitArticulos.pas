unit UKitArticulos;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.FMXUI.Wait,
  FMX.Controls.Presentation, FMX.StdCtrls, Data.DB, FireDAC.Comp.Client,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox;

type
  TKitArticulos = class(TForm)
    Conexion: TFDConnection;
    btnGuardar: TButton;
    btnCancelar: TButton;
    sgArticulos: TStringGrid;
    CLAVE: TStringColumn;
    ARTICULO: TStringColumn;
    UNIDAD: TStringColumn;
    PRECIO: TStringColumn;
    TOTAL: TStringColumn;
    Label1: TLabel;
    procedure btnGuardarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure sgArticulosEditingDone(Sender: TObject; const ACol,
      ARow: Integer);
  private
    { Private declarations }
  public
  art_id:integer;
    { Public declarations }
  end;

var
  KitArticulos: TKitArticulos;

implementation

{$R *.fmx}

procedure TKitArticulos.btnCancelarClick(Sender: TObject);
begin
ModalResult:=mrCancel;
self.Close;
end;

procedure TKitArticulos.btnGuardarClick(Sender: TObject);
var
clave_art_id,componente_id,i,unidades:integer;
clave_art:string;
begin
clave_art:=Conexion.ExecSQLScalar('select clave_articulo from get_clave_art(:art)',[inttostr(art_id)]);
clave_art_id:=Conexion.ExecSQLScalar('select clave_articulo_id from claves_articulos where clave_articulo=:cla',[clave_art]);
for i := 0 to sgArticulos.RowCount-2 do
 begin
  componente_id:=Conexion.ExecSQLScalar('select articulo_id from articulos where nombre=:n',[sgArticulos.Cells[1,i]]);
  unidades:=strtoint(sgArticulos.Cells[2,i]);
  Conexion.ExecSQL('insert into juegos_det(ARTICULO_ID,CLAVE_ARTICULO_ID,COMPONENTE_ID,UNIDADES) values(:p1,:p2,:p3,:p4)',[inttostr(art_id),inttostr(clave_art_id),componente_id,unidades]);
 end;
 ShowMessage('Kit creado Correctamente.');
ModalResult:=mrOk;
//self.Close;
end;

procedure TKitArticulos.sgArticulosEditingDone(Sender: TObject; const ACol,
  ARow: Integer);
var
id_articulo:Integer;
precio,unidades:Double;
valor:string;
name_column:string;
begin
valor:=sgArticulos.Cells[ACol,ARow];
name_column:=sgArticulos.Columns[Acol].Name;

if valor <> '' then
  begin
  //Si es clave
  if name_column = 'CLAVE' then
    begin
      id_articulo:=Conexion.ExecSQLScalar('SELECT articulo_id from ORSP_BUSCA_CLAVE_ARTICULO('''+valor+''')');
      if id_articulo = 0 then
      begin
      ShowMessage('Articulo no existe');
      sgArticulos.Cells[Acol,ARow]:='';
      end
      else
      begin
      precio:=Conexion.ExecSQLScalar('SELECT pa.precio FROM precios_articulos pa left join precios_empresa pe on pa.precio_empresa_id=pe.precio_empresa_id where pa.articulo_id='+id_articulo.ToString+' and pe.nombre=''Precio de lista''');
      if precio = Null then precio:=0;

      if sgArticulos.Cells[Acol+1,ARow] = '' then sgArticulos.RowCount:= sgArticulos.RowCount+1;
      sgArticulos.Cells[Acol+1,ARow]:= Conexion.ExecSQLScalar('select NOMBRE from ARTICULOS where articulo_id='+id_articulo.ToString+'');
      if sgArticulos.Cells[Acol+3,ARow] = '' then sgArticulos.Cells[Acol+3,ARow]:= precio.ToString;
      if sgArticulos.Cells[Acol+2,ARow] = '' then sgArticulos.Cells[Acol+2,ARow]:= '1';
      if sgArticulos.Cells[Acol+4,ARow] = '' then sgArticulos.Cells[Acol+4,ARow]:= precio.ToString;

      end;
    end;
  //si es nombre
  if name_column = 'ARTICULO' then
  begin
    id_articulo:=Conexion.ExecSQLScalar('select articulo_id from ARTICULOS where nombre='''+valor+'''');
    if id_articulo = 0 then
      begin
      ShowMessage('Articulo no existe');
      sgArticulos.Cells[Acol,ARow]:='';
      end
    else
      begin
      precio:=Conexion.ExecSQLScalar('SELECT pa.precio FROM precios_articulos pa left join precios_empresa pe on pa.precio_empresa_id=pe.precio_empresa_id where pa.articulo_id='+id_articulo.ToString+' and pe.nombre=''Precio de lista''');
      if precio = Null then precio:=0;

      if sgArticulos.Cells[Acol-1,ARow] = '' then sgArticulos.RowCount:= sgArticulos.RowCount+1;
       try
        sgArticulos.Cells[Acol-1,ARow]:= Conexion.ExecSQLScalar('SELECT coalesce(CLAVE_ARTICULO,''Sin Clave'') as CLAVE_ARTICULO from GET_CLAVE_ART('+id_articulo.ToString+')');
       finally
       end;
      if sgArticulos.Cells[Acol+2,ARow] = '' then sgArticulos.Cells[Acol+2,ARow]:= precio.ToString;
      if sgArticulos.Cells[Acol+1,ARow] = '' then  sgArticulos.Cells[Acol+1,ARow]:= '1';
      if sgArticulos.Cells[Acol+3,ARow] = '' then sgArticulos.Cells[Acol+3,ARow]:= precio.ToString;


      end;
  end;
  //si es unidad
  if name_column = 'UNIDAD' then
    begin
    if TryStrToFloat(sgArticulos.Cells[Acol,ARow],unidades) = False then
      begin
        sgArticulos.Cells[Acol,ARow]:='1';
      end
    else
      begin
        precio:=sgArticulos.Cells[Acol+1,ARow].ToDouble;
        unidades:= sgArticulos.Cells[Acol,ARow].ToDouble;
        sgArticulos.Cells[Acol+2,ARow]:=(unidades*precio).ToString;
      end;
    end;
    //si es precio
  if name_column = 'PRECIO' then
    begin
    precio:=sgArticulos.Cells[Acol,ARow].ToDouble;
    unidades:= sgArticulos.Cells[Acol-1,ARow].ToDouble;
    sgArticulos.Cells[Acol+1,ARow]:=(unidades*precio).ToString;
    end;


  end;

end;

end.
