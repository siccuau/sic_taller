unit UConexiones;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls, Vcl.Dialogs,
  FMX.ListBox, FMX.Controls.Presentation, FMX.Edit,registry,Winapi.Windows, Winapi.Messages;

type
  TConexiones = class(TForm)
    txtNombre: TEdit;
    txtServidor: TEdit;
    txtCarpetaMSP: TEdit;
    btnAceptar: TButton;
    btnCancelar: TButton;
    btnEliminar: TButton;
    cb_tipo_conexion: TComboBox;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label2: TLabel;
    ListBoxItem1: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure cb_tipo_conexionChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Conexiones: TConexiones;

implementation

{$R *.fmx}

procedure TConexiones.btnAceptarClick(Sender: TObject);
var
  reg:TRegistry;
begin
  reg:=TRegistry.Create(KEY_WRITE OR KEY_WOW64_64KEY);
  reg.RootKey := HKEY_LOCAL_MACHINE;
  if (not reg.OpenKey('SOFTWARE\SIC\Conexiones\'+txtNombre.Text,False)) then
  begin
    reg.CreateKey('SOFTWARE\SIC\Conexiones\'+txtNombre.Text);
  end
  else
  begin
     reg.OpenKeyReadOnly('SOFTWARE\SIC\Conexiones\'+txtNombre.Text)
  end;
  reg.OpenKey('SOFTWARE\SIC\Conexiones\'+txtNombre.Text, True);
  reg.WriteString('Datos',txtCarpetaMSP.Text);
  reg.WriteString('Servidor',txtServidor.Text);
  reg.WriteString('Tipo',cb_tipo_conexion.Items.Text);
  ModalResult := mrOK;
end;

procedure TConexiones.btnCancelarClick(Sender: TObject);
begin
Close;
end;

procedure TConexiones.btnEliminarClick(Sender: TObject);
var
  reg:TRegistry;
begin
   reg:=TRegistry.Create(KEY_WRITE OR KEY_WOW64_64KEY);
   reg.RootKey := HKEY_LOCAL_MACHINE;
   if messagedlg('Desea eliminar esta Conexion?',mtError, mbOKCancel, 0)= mrOK then
   begin
      reg.DeleteKey('SOFTWARE\SIC\Conexiones\'+txtNombre.Text);
      ShowMessage('Conexion Eliminada');
      ModalResult := mrOK;
   end;
end;

procedure TConexiones.cb_tipo_conexionChange(Sender: TObject);
begin
  if (cb_tipo_conexion.Items.Strings[cb_tipo_conexion.ItemIndex] = 'Local') then
  begin
    Label3.Enabled:=False;
    txtServidor.Enabled:=False;
  end
  else
  begin
    Label3.Enabled:=True;
    txtServidor.Enabled:=True;
  end;
end;

end.
