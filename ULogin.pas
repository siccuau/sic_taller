unit ULogin;

interface

uses
  Winapi.Windows, Winapi.Messages,System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,registry, Vcl.Dialogs ,
  FMX.ListBox, FMX.Edit, FMX.Objects, FMX.Controls.Presentation,UConexiones,USeleccionaEmpresa,
  FMX.Menus,ULicencia, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.FMXUI.Wait, Data.DB, FireDAC.Comp.Client;

type
  TLogin = class(TForm)
    Label1: TLabel;
    Image1: TImage;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    txtPassword: TEdit;
    txtUsername: TEdit;
    cb_conexiones: TComboBox;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    PopupMenu1: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    FDConnection: TFDConnection;
    procedure MenuItem1Click(Sender: TObject);
       procedure load_conexiones();
    procedure MenuItem2Click(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure txtPasswordKeyUp(Sender: TObject; var Key: Word;
      var KeyChar: Char; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Login: TLogin;

implementation

{$R *.fmx}

procedure TLogin.BtnAceptarClick(Sender: TObject);
var
  reg:TRegistry;
  msg:String;
  FormSeleccionaEmpresa:TSeleccionaEmpresa;
  lic : TLicencia;
begin
  lic := TLicencia.Create;
  lic.SetGlobalEnvironment('ultima_conexion_sic',inttostr(cb_conexiones.ItemIndex)+';'+txtUsername.Text);
  //SE OBTIENEN LOS DATOS DE LA CONEXION
  reg := TRegistry.Create(KEY_WRITE OR KEY_WOW64_64KEY);
  reg.RootKey := HKEY_LOCAL_MACHINE;
  if (reg.KeyExists('SOFTWARE\SIC\Conexiones\'+cb_conexiones.Items.Strings[cb_conexiones.ItemIndex])) then
  begin
    reg.OpenKeyReadOnly('SOFTWARE\SIC\Conexiones\'+cb_conexiones.Items.Strings[cb_conexiones.ItemIndex]);
    if (reg.ReadString('Servidor')= '') then
     begin
        FDConnection.Params.Values['Server'] := 'localhost';
     end
     else
     begin
        FDConnection.Params.Values['Server'] := reg.ReadString('Servidor');
     end;
    FDConnection.Params.Values['Database'] := reg.ReadString('Datos')+'System\config.fdb';
    FDConnection.Params.Values['User_Name'] := txtUserName.Text;
    FDConnection.Params.Values['Password'] := txtPassword.Text;
    try
     FDConnection.Connected := True;

     FormSeleccionaEmpresa := TSeleccionaEmpresa.Create(nil);
     if (reg.ReadString('Servidor')= '') then
     begin
        FormSeleccionaEmpresa.FDConnection.Params.Values['Server'] := 'localhost';
     end
     else
     begin
        FormSeleccionaEmpresa.FDConnection.Params.Values['Server'] := reg.ReadString('Servidor');
     end;

     FormSeleccionaEmpresa.FDConnection.Params.Values['Database'] := reg.ReadString('Datos')+'System\config.fdb';
     FormSeleccionaEmpresa.FDConnection.Params.Values['User_Name'] := txtUserName.Text;
     FormSeleccionaEmpresa.FDConnection.Params.Values['Password'] := txtPassword.Text;
     FormSeleccionaEmpresa.FDConnection.Connected := True;
     Hide;
     FormSeleccionaEmpresa.Show;
     FDConnection.Connected := False;
    except
      msg := 'El nombre de usuario o la contrase�a no son v�lidos para el servidor de la conexion "'+cb_conexiones.Items.Strings[cb_conexiones.ItemIndex]+'".'+#13#10+'Escriba los datos correctamente o consulte al Administrador del sistema.';
      MessageDlg(msg, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TLogin.FormShow(Sender: TObject);
var
  lic : TLicencia;
  conexionIndex : integer;
  conexionUsuario:string;
begin
  lic := Tlicencia.Create;
  try
     conexionIndex := strtoint(lic.GetEnvVariable('ultima_conexion_sic').split([';'])[0]);
     conexionUsuario := UpperCase(lic.GetEnvVariable('ultima_conexion_sic').split([';'])[1]);
  Except
     conexionIndex := 0;
     conexionUsuario := 'SYSDBA'
  end;

  load_conexiones;
  cb_conexiones.ItemIndex := conexionIndex;
  txtUsername.Text := conexionUsuario;
  txtPassword.SetFocus;
end;

procedure TLogin.MenuItem1Click(Sender: TObject);
var
  FormConexion:TConexiones;
  reg:TRegistry;
begin
  FormConexion:=Tconexiones.Create(nil);
  //SE OBTIENEN LOS DATOS DE LA CONEXION
  reg := TRegistry.Create(KEY_WRITE OR KEY_WOW64_64KEY);
  reg.RootKey := HKEY_LOCAL_MACHINE;
  if (reg.KeyExists('SOFTWARE\SIC\Conexiones\'+cb_conexiones.Items.Strings[cb_conexiones.ItemIndex])) then
  begin
    reg.OpenKeyReadOnly('SOFTWARE\SIC\Conexiones\'+cb_conexiones.Items.Strings[cb_conexiones.ItemIndex]);
    FormConexion.txtCarpetaMSP.Text:=reg.ReadString('Datos');
    FormConexion.txtServidor.Text := reg.ReadString('Servidor');
    FormConexion.txtNombre.Text := cb_conexiones.Items.Strings[cb_conexiones.ItemIndex];
    FormConexion.txtNombre.Enabled:=False;
    FormConexion.cb_tipo_conexion.Enabled := False;
    if (reg.ReadString('Tipo') = 'Remoto') then
    begin
      FormConexion.txtServidor.Enabled:=True;
      FormConexion.cb_tipo_conexion.ItemIndex := 1;
    end
    else
    begin
      FormConexion.txtServidor.Enabled:=False;
      FormConexion.cb_tipo_conexion.ItemIndex := 0;
    end;
  end;
  FormConexion.Caption:='Abrir Conexion';
  if FormConexion.ShowModal = mrOK then load_conexiones;
end;

procedure TLogin.MenuItem2Click(Sender: TObject);
var
  FormConexion:TConexiones;
begin
  FormConexion:=Tconexiones.Create(nil);
  FormConexion.Caption:='Nueva Conexion';
  if FormConexion.ShowModal = mrOK then load_conexiones;
end;

procedure TLogin.txtPasswordKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  if Key = VK_RETURN then  BtnAceptar.SetFocus;
end;

procedure TLogin.load_conexiones();
var
  reg:TRegistry;
  Conexiones:TStringList;
  Conexion:String;
  FormConexion:TConexiones;
  begin
   cb_conexiones.Items.Clear;
    reg := TRegistry.Create(KEY_WRITE OR KEY_WOW64_64KEY);
    reg.RootKey := HKEY_LOCAL_MACHINE;

    // AGREGA LAS CONEXIONES EXISTENTES AL COMBO DE LA PANTALLA
    if (reg.KeyExists('SOFTWARE\SIC\Conexiones')) then
    begin
      reg.OpenKeyReadOnly('SOFTWARE\SIC\Conexiones');
      if reg.HasSubKeys then
      begin
        Conexiones:=TStringList.Create;
        reg.GetKeyNames(Conexiones);
        for Conexion in Conexiones do
        begin
          //cb_conexiones.AddObject(Conexion,nil);
          cb_conexiones.Items.Add(Conexion);
        end;
        cb_conexiones.ItemIndex := 0;
      end
      else
      begin
        MessageDlg('No hay Conexiones SIC creadas.', mtError, [mbOK], 0);
        FormConexion:=TConexiones.Create(nil);
        FormConexion.Caption:='Nueva Conexion';
        FormConexion.ShowModal;
      end;
    end
    else
    begin
      MessageDlg('No hay Conexiones SIC creadas.', mtError, [mbOK], 0);
      FormConexion:=TConexiones.Create(nil);
      FormConexion.Caption:='Nueva Conexion';
      FormConexion.ShowModal;
    end;
  end;
end.
